#!/bin/bash

wget https://bootstrap.pypa.io/get-pip.py
python get-pip.py

/usr/local/bin/pip install boto3

wget https://bitbucket.org/spindox_cloud_factory/python_tags_asg/raw/master/wls-asg.py

sleep $[ ( $RANDOM % 10 ) + ( $RANDOM % 5 ) + ($RANDOM % 5) ]s
export SERVER=$(python wls-asg.py)

echo $SERVER > /tmp/debug.txt

nohup /root/Oracle/Middleware/wlserver_10.3/common/bin/startManagedWebLogic.sh $SERVER 'http://admin.weblogic.local:7001' &