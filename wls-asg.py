import boto3
import urllib2
import os

available = ['Server-0', 'Server-1', 'Server-2', 'Server-3', 'Server-4', 'Server-5', 'Server-6', 'Server-7', 'Server-8', 'Server-9']
instanceId = urllib2.urlopen('http://169.254.169.254/latest/meta-data/instance-id').read()

client = boto3.client(
    'ec2',
    region_name='eu-central-1'
)

response = client.describe_instances(
        Filters=[
        {
            'Name': 'tag:aws:autoscaling:groupName',
            'Values': ['wls-asg']
        }
    ]
)

reservations = response['Reservations']

def remove_duplicated ( instance_id ):
    ec2 = boto3.resource(
        'ec2',
        region_name='eu-central-1'
    )
    instance = ec2.Instance(instance_id)
    instance.terminate(
        DryRun=False
    )

for i in range(len(reservations)) :
    instances = reservations[i]['Instances']
    for j in range(len(instances)) :
        tags = instances[j]['Tags']
        for k in range(len(tags)) :
            if tags[k]['Key'] == 'weblogic-name' and instances[j]['State']['Name'] == 'running' :
                try:
                    available.remove(tags[k]['Value'])
                except:
                    remove_duplicated(instances[j]['InstanceId'])

if available[0] :
    client.create_tags(
        DryRun=False,
        Resources=[
            instanceId
        ],
        Tags=[
            {
                'Key': 'weblogic-name',
                'Value': available[0]
            }
        ]
    )
    os.environ["SERVER"] = available[0]
    print available[0]